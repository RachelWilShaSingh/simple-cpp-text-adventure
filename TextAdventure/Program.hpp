#ifndef PROGRAM_HPP
#define PROGRAM_HPP

#include <iostream>
#include <fstream>
#include <map>
#include <string>
using namespace std;

#include "Room.hpp"
#include "Logger.hpp"

class Program
{
    public:
    Program();
    ~Program();

    void Run();

    private:
    bool SetupRooms();
    void CreateRoom( string id, string name, string description, string north, string south, string east, string west );
    string HandleUserInput();
    Room* m_ptrCurrentRoom;
    map<string, Room*> m_rooms;
    bool m_done;
    string m_endRoomId;
};

#endif
