#ifndef ROOM_HPP
#define ROOM_HPP

#include <iostream>
#include <string>
using namespace std;

#include "Utilities.hpp"
#include "enums.hpp"


struct Room
{
    Room();
    Room( string id, string name, string description );
    void SetNeighbors( string north, string south, string east, string west );
    void Setup( string id = "", string name = "", string description = "" );

    void OutputRoomInfo();
    void OutputNeighbors();
    bool CanGo( Direction direction );
    string GetNeighborId( Direction direction );
    string GetId();

    string id;
    string name;
    string description;
    string neighborIds[4];
};


#endif
