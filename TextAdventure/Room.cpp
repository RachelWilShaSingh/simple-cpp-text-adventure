#include "Room.hpp"


Room::Room()
{
    Setup();
}

Room::Room( string id, string name, string description )
{
    Setup( id, name, description );
}

void Room::Setup( string id /* = "" */, string name /* = ""*/, string description /* = "" */ )
{
    this->id = id;
    this->name = name;
    this->description = description;

    for ( int i = 0; i < 4; i++ )
    {
        neighborIds[i] = "NULL";
    }
}

void Room::SetNeighbors( string north, string south, string east, string west )
{
    neighborIds[NORTH] = north;
    neighborIds[SOUTH] = south;
    neighborIds[EAST] = east;
    neighborIds[WEST] = west;
}

void Room::OutputRoomInfo()
{
    Menu::ClearScreen();
    Menu::Header( name );
    cout << "\t" << description << endl;
}

void Room::OutputNeighbors()
{
    cout << "\t You can go: ";

    for ( int i = 0; i < 4; i++ )
    {
        if ( neighborIds[i] != "NULL" )
        {
            cout << GetDirectionString( Direction( i ) ) << " ";
        }
    }

    cout << endl;
}

bool Room::CanGo( Direction direction )
{
    for ( int i = 0; i < 4; i++ )
    {
        if ( direction == Direction(i) && neighborIds[i] != "NULL" )
        {
            return true;
        }
    }

    return false;
}

string Room::GetNeighborId( Direction direction )
{
    return neighborIds[ direction ];
}

string Room::GetId()
{
    return id;
}
